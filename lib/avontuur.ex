defmodule Avontuur do
  @moduledoc """
  An interactive console based adventure game.

  Define your own adventures! Look at `adventures/testje.exs` and
  the module docs of `Game`, `Room` and `Item` for more information.
  """

  alias Avontuur.Game

  @helptext """
  Je kunt de volgende opdrachten gebruiken:

  help
  kijk
  kijk / bekijk <iets>
  rugzak
  pak <iets>
  weg <iets>
  gebruik / eet <iets>
  ga <richting>
  stop

  De richtingen zijn noord, oost, zuid, west, boven, beneden.
  Afgekort is dat: n, o, z, w. Boven en beneden zijn niet afgekort.
  """

  @doc """
  Start a new adventure as described in `filename`.
  """
  def main(args) do
    [filename] = args

    {game, _} = Code.eval_file(filename)

    game
    |> Game.start_game()
    |> loop()

    :ok
  end

  def loop(game) do
    if game.game_over, do: exit(:normal)

    # Match commands and normalize direction arguments
    input =
      IO.gets("> ")
      |> String.trim_trailing()
      |> String.split(" ", parts: 2)
      |> case do
        [cmd] -> [find_command(cmd)]
        [cmd, arg] ->
          cmd = find_command(cmd)
          arg = normalize_argument(cmd, arg)
          [cmd, arg]
      end

    # Execute the command and start another loop
    case input do
      ["help"] ->
        Game.text(game, @helptext)
      ["look"] ->
        Game.look(game)
      ["look", item] ->
        Game.look(game, item)
      ["inventory"] ->
        Game.inventory(game)
      ["pickup", item] ->
        Game.pickup(game, item)
      ["drop", item] ->
        Game.drop(game, item)
      ["move", direction] ->
        Game.move(game, direction)
      ["use", item] ->
        Game.use(game, item)
      ["stop"] ->
        Game.quit_game(game, "Bedankt voor het spelen!")
      _ ->
        Game.text(game, "Sorry, dat begrijp ik niet.\n")
    end
    |> loop()
  end

  defp find_command(cmd) do
    cond do
      String.contains? cmd, ["kijk", "bekijk"] -> "look"
      String.contains? cmd, ["inv", "rugzak"] -> "inventory"
      String.contains? cmd, ["get", "pak"] -> "pickup"
      String.contains? cmd, ["weg"] -> "drop"
      String.contains? cmd, ["go", "ga"] -> "move"
      String.contains? cmd, ["eat", "gebruik", "eet"] -> "use"
      String.contains? cmd, ["exit", "quit"] -> "stop"
      true -> cmd
    end
  end

  defp normalize_argument("move", dir) do
    case dir do
      "n" -> "north"
      "e" -> "east"
      "s" -> "south"
      "w" -> "west"
      "o" -> "east"
      "z" -> "south"
      "noord"   -> "north"
      "oost"    -> "east"
      "zuid"    -> "south"
      "boven"   -> "up"
      "beneden" -> "down"
      _   -> dir
    end
  end
  defp normalize_argument(_cmd, arg), do: arg
end
