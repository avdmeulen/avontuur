defmodule Avontuur.Room do
  @moduledoc """
  A room has a unique id (atom), name and description (strings).

  Exits are atom values that represent the room that is connected in a direction.

  A room holds items, represented as atom values.

  Callback functions can be defined for `look`, `enter` and `leave`. These
  methods will be called when the user uses the `look` or `move` commands.
  These callbacks accept the current gamestate and must return the
  (modified) gamestate.

  Write custom callbacks to add more dynamics to your story.
  Use the non-user and util commands from the `Game` module to query or
  modify the current gamestate.
  """

  defstruct [
    id: nil,
    name: nil,
    desc: nil,
    north: nil, east: nil, south: nil, west: nil, up: nil, down: nil,
    items: [],
    look: nil,
    enter: nil,
    leave: nil,
  ]

  @doc """
  All the exits that connect to another room.

  Returns a Keyword like [south: :tuin]
  """
  def exits(room) do
    [:north, :east, :south, :west, :up, :down]
    |> Enum.reduce([], fn (dir, acc) -> add_exit_if_present(dir, room, acc) end)
  end

  defp add_exit_if_present(direction, room, exits) do
    case Map.get(room, direction) do
      nil -> exits
      room -> [{direction, room} | exits]
    end
  end
end
