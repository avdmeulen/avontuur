defmodule Avontuur.Item do
  @moduledoc """
  An item has a unique id (atom), name and description (strings).

  Callback functions can be defined for `look` and `use`. These
  methods will be called when the user uses the `look` or `use` commands.
  These callbacks accept the current gamestate and must return the
  (modified) gamestate.

  Write custom callbacks to add more dynamics to your story.
  Use the non-user and util commands from the `Game` module to query or
  modify the current gamestate.
  """

  defstruct [
    id: nil,
    name: nil,
    desc: nil,
    look: nil,
    use: nil,
  ]
end
