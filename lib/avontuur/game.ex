defmodule Avontuur.Game do
  @moduledoc """
  The gamestate and commands to modify the gamestate.

  Each command accepts the current gamestate and returns the
  new gamestate.

  The gamestate consists of items, rooms and inventory. The player
  is always present in a room, the `curr_room`.

  Look at `Item` and `Room` modules for more information.
  """

  alias Avontuur.{Game, Room}

  defstruct [
    items: [],
    rooms: [],
    inventory: [],         # ids
    curr_room: nil,        # id
    prev_room: nil,        # id
    game_over: false,
    desc: nil,             # text displayed at start of game
    data: %{},             # custom game variables
  ]

  # START/STOP

  def start_game(%Game{game_over: false} = game) do
    room = current_room(game)
    game =
      game
      |> text("Hallo daar, avonturier.\n")
      |> text(game.desc)
      |> inventory()
      |> look()
      |> text("Type 'help' voor de beschikbare commandos.\n")
    case room.enter do
      nil -> game
      fun -> fun.(game)
    end
  end

  def quit_game(%Game{game_over: false} = game, message \\ false) do
    if message, do: text(message)
    %{game | game_over: true}
  end

  # USER COMMANDS

  def look(%Game{} = game) do
    room = current_room(game)
    text "\n== #{String.upcase(room.name)} ==\n"
    case room.look do
      nil ->
        game
        |> text(room.desc)
        |> describe_items_in_room()
        |> describe_exits_in_room()
      fun ->
        fun.(game) # Custom look function
    end
  end

  def look(%Game{} = game, item_name) do
    room = current_room(game)
    item_id = find_item(room.items, item_name) || find_item(game.inventory, item_name)
    case item_id do
      nil ->
        text game, "Ik weet niet wat een #{item_name} is.\n"
      _ ->
        item = get_item(game, item_id)
        case item.look do
          nil -> text game, "#{item.desc}\n"
          fun -> fun.(game) # Custom look function
        end
    end
  end

  def move(%Game{} = game, direction) when direction in ~w/north east south west up down/ do
    room = current_room(game)
    next_room = get_room(game, Map.get(room, String.to_atom(direction)))
    case next_room do
      nil ->
        text game, "Daar kun je helemaal niet heen.\n"
      _ ->
        game = case room.leave do
          nil -> game
          fun -> fun.(game)
        end
        |> move_to_room(next_room.id)
        |> look()

        case next_room.enter do
          nil -> game
          fun -> fun.(game)
        end
    end
  end

  def move(%Game{} = game, _) do
    text game, "Die richting ken ik niet"
  end

  def pickup(%Game{} = game, item_name) do
    room = current_room(game)
    item_id = find_item(room.items, item_name)
    case item_id do
      nil ->
        text game, "Er ligt hier geen #{item_name}.\n"
      _ ->
        game
        |> remove_from_room(item_id)
        |> add_to_inventory(item_id)
        |> text("Je hebt een #{item_name} opgepakt.\n")
    end
  end

  def drop(%Game{inventory: []} = game, item_name) do
    text game, "Je hebt helemaal geen #{item_name}.\n"
  end

  def drop(%Game{inventory: inventory} = game, item_name) do
    item_id = find_item(inventory, item_name)
    case item_id do
      nil ->
        text game, "Je hebt helemaal geen #{item_name}.\n"
      _ ->
        game
        |> add_to_room(item_id)
        |> remove_from_inventory(item_id)
        |> text("Je hebt een #{item_name} laten vallen.\n")
    end
  end

  def inventory(%Game{inventory: []} = game) do
    text game, "Je hebt niks bij je.\n"
  end

  def inventory(%Game{inventory: inventory} = game) do
    Enum.reduce(inventory, game, fn item_id, _acc ->
      item = get_item(game, item_id)
      case item.look do
        nil -> text game, "#{item.name} - #{item.desc}"
        fun -> fun.(game) # Custom look function
      end
    end)
  end

  def use(%Game{inventory: inventory} = game, item_name) do
    # room = current_room(game)
    # item_id = find_item(room.items, item_name) || find_item(inventory, item_name)
    item_id = find_item(inventory, item_name)
    case item_id do
      nil -> text game, "Ik weet niet wat een #{item_name} is.\n"
      _ ->
        item = get_item(game, item_id)
        case item.use do
          nil -> text game, "Ik weet niet hoe dat moet met een #{item_name}.\n"
          fun -> fun.(game)
        end
    end
  end

  ## NON-USER COMMANDS

  def move_to_room(%Game{curr_room: curr_room} = game, room_id) do
    %{game | curr_room: room_id, prev_room: curr_room}
  end

  def add_to_inventory(%Game{inventory: inventory} = game, item_id) do
    %{game | inventory: [item_id | inventory]}
  end

  def remove_from_inventory(%Game{inventory: inventory} = game, item_id) do
    %{game | inventory: List.delete(inventory, item_id)}
  end

  def add_to_room(%Game{} = game, item_id, room_id \\ nil) do
    room = get_room(game, room_id || game.curr_room)
    update_room(game, %{room | items: [item_id | room.items]})
  end

  def remove_from_room(%Game{} = game, item_id, room_id \\ nil) do
    room = get_room(game, room_id || game.curr_room)
    update_room(game, %{room | items: List.delete(room.items, item_id)})
  end

  def update_room(%Game{rooms: rooms} = game, room) do
    room_idx = Enum.find_index(rooms, fn(r) -> r.id == room.id end)
    %{game | rooms: List.replace_at(rooms, room_idx, room)}
  end

  def text(str), do: IO.puts(str)
  def text(%Game{} = game, str) do
    text(str)
    game
  end

  def describe_items_in_room(%Game{} = game, room_id \\ nil) do
    %Room{items: items} = get_room(game, room_id || game.curr_room)
    if length(items) > 0 do
      text "Je ziet hier:"
      Enum.each(items, fn item_id -> text "- #{get_item(game, item_id).name}" end)
      text ""
    end
    game
  end

  def describe_exits_in_room(%Game{} = game, room_id \\ nil) do
    room = get_room(game, room_id || game.curr_room)
    text "Je kan de volgende kanten op:"
    Room.exits(room)
    |> Enum.each(fn {dir, room_id} ->
      text "- #{dir} : #{get_room(game, room_id).name}"
    end)
    text game, ""
  end

  # UTILS

  def current_room(%Game{} = game) do
    get_room(game, game.curr_room)
  end

  def get_room(%Game{} = game, room_id) do
    Enum.find(game.rooms, fn room -> room.id == room_id end)
  end

  def find_item(items, item_name) do
    Enum.find(items, fn item_id -> Atom.to_string(item_id) == item_name end)
  end

  def get_item(%Game{} = game, item_id) do
    Enum.find(game.items, fn item -> item.id == item_id end)
  end

  def user_has_item(%Game{} = game, item_id) do
    item_id in game.inventory
  end
end
