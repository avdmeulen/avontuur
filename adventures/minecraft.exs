# Een minecraft avontuur

alias Avontuur.{Game, Item, Room}

# Define the items

grote_steen = %Item{
  id: :grote_steen,
  name: "grote steen",
  desc: "Een hele grote steen die nogal in de weg ligt."
}

houweel = %Item{
  id: :houweel,
  name: "houweel",
  desc: "Een pikhouweel, handig om stenen mee te verbrijzelen.",
  use: fn game ->
    case game.curr_room do
      :huis ->
        room = Game.get_room(game, :huis)
        game
        |> Game.update_room(%{room | west: :mijn_ingang})
        |> Game.text("""
          Je hakt de steen in honderd miljoen miljard stukjes.
          Als het stof is opgetrokken zie je een geheime ingang.
          """)
      _ ->
        Game.text(game, "Je ziet hier niets om lekker op te gaan hakken.")
    end
  end
}

fakkel = %Item{
  id: :fakkel,
  name: "fakkel",
  desc: "De fakkel brandt fel, heel handig in het donker."
}

# Define the rooms

huis = %Room{
  id: :huis,
  name: "huis",
  desc: """
  Dit is je huis.
  Het zonlicht valt door het raam in de deur naar binnen.
  Je ziet een bed staan, een werkbank en een kist.
  Aan de muur hangt een schilderij van een zwaard.
  """,
  east: :grasveld,
  enter: fn game ->
    game = update_in(game.data.houseVisits, &(&1 + 1))
    Game.text(game, "Je ben hier nu #{game.data.houseVisits} keer geweest")
  end,
  look: fn game ->
    room = Game.current_room(game)
    maybe_desc_rock = fn game ->
      case room.west do
        :mijn_ingang ->
          game
        _ ->
          Game.text(game, """
            Er ligt een grote rots tegen de muur, het lijkt of er een ingang achter zit.
            Je probeert het gesteente weg te rollen maar het lukt niet.
            """)
      end
    end

    game
    |> Game.text(room.desc)
    |> maybe_desc_rock.()
    |> Game.describe_items_in_room()
    |> Game.describe_exits_in_room()
  end
}

mijn_ingang = %Room{
  id: :mijn_ingang,
  name: "mijn ingang",
  desc: """
  Je staat voor de ingang van een lange mijnschacht.
  Uit de ondergrondse tunnels komen rare geluiden vandaan.
  Zitten er soms monsters verstopt?
  """,
  east: :huis
}

grasveld = %Room{
  id: :grasveld,
  name: "grasveld",
  desc: """
  Je staat in een grasveld met bomen en bloemen.
  Er staat een stenen huis met een houten dak, het is jouw huis.
  Een zandpad kronkelt naar het bos, verderop zie je het strand liggen.
  """,
  north: :bos,
  west: :huis,
  south: :strand
}

bos = %Room{
  id: :bos,
  name: "bos",
  desc: """
  Je loopt door een prachtig bos.
  In de verte zie je een konijntje huppelen.
  """,
  items: [:houweel],
  south: :grasveld
}

strand = %Room{
  id: :strand,
  name: "strand",
  desc: """
  Je loopt over het goudgele zand van het brede strand.
  """,
  items: [:fakkel],
  north: :grasveld
}

# Return the initial gamestate

%Game{
  items: [grote_steen, houweel, fakkel],
  rooms: [huis, grasveld, bos, mijn_ingang, strand],
  curr_room: :huis,
  desc: """
  Wat is dit? De hele wereld lijkt wel gemaakt van blokken?!
  """,
  data: %{
    houseVisits: 0 # testing
  }
}
