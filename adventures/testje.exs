# An example adventure. Eat the ice cream and you're done.

alias Avontuur.{Game, Item, Room}

# Define the items

lepel = %Item{
  id: :lepel,
  name: "lepel",
  desc: "Een hele gewone lepel"
}

ijs = %Item{
  id: :ijs,
  name: "ijs",
  desc: "Een grote bak met aardbeienijs.",
  use: fn game ->
    # User must have spoon and icecream in the inventory to eat the icecream.
    if Game.user_has_item(game, :lepel) do
      game
      |> Game.text("Je eet het aardbeienijs langzaam op. Het is ontzettend lekker.")
      |> Game.remove_from_inventory(:ijs)
      |> Game.quit_game("Met het eten van het ijsje is je avontuur afgelopen.")
    else
      Game.text(game, "Het ijs is te koud om met je handen te eten, je hebt iets anders nodig.")
    end
  end
}

# Define the rooms

keuken = %Room{
  id: :keuken,
  name: "keuken",
  desc: "Een grote keuken met uitzicht over de tuin.",
  south: :tuin,
  items: [:ijs],
  look: nil,
  enter: fn game -> Game.text("Welkom in de keuken."); game end,
  leave: nil,
}

tuin = %Room{
  id: :tuin,
  name: "tuin",
  desc: "Een prachtige tuin met veel bloemen en planten.",
  north: :keuken,
  items: [:lepel],
}

# Return the initial gamestate

%Game{
  items: [lepel, ijs],
  rooms: [keuken, tuin],
  curr_room: :keuken
}
