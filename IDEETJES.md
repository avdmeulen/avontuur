Avontuur spel voor Jelle

Text adventure, samen bedenken en spelen.
Console based.
Je moet de uitgang zien te vinden.
Spullen zoeken en gebruiken, puzzeltjes oplossen.
Meer van hetzelfde kunnen verzamelen.
NPC, ask/tell

 F    U
L R
 B    D


- player
  - inventory
- rooms
- objects/items (in room of in inventory)
- obstacles/doors

objects:
- look
- use
- pick up
- drop



Code

DSL
    use Avontuur.DSL

    room :keuken do
      name "Keuken"
      desc "Blabla keuken"

      north :hal
      east :eetkamer
      south :tuin

      object "lepel", "Een hele mooie lepel"
      object "ijs", "Een bak met citroen ijs" do
        use do
          if User.has_item?(user, :lepel) do # if User.item_count(user, :lepel) > 0
            text "Hmm, dat ijs is echt lekker!"
            User.dec_item_count(user, :lepel, 1)
          else

          end

          # of
        end
      end

      object :koelkast do
        name "koelkast" # wordt afgeleid van key indien niet opgegeven
        desc "Een koelkast met een vriesvak, er lijkt iets in te zitten"
        pickup, do: false # kan niet worden opgepakt
        pickup false

        pickup do
          if Item.
        end
      end

      enter do
        # doe iets als de speler deze ruimte binnen komt
      end

      leave do
        # doe iets als de speler deze ruimte verlaat
      end

      npc :wizard do
        name "Tovenaar Merlijn"
        desc "Deze beroemde tovenaar draag een lange mantel en een gedeukte puntmuts."

        enter do
          answer = ask "Ik ben de grote tovenaar Merlijn. Voor je verder mag etc. Hoeveel is 30 / 6?"

          if answer == 5 do
            text "Ok, kom verder"
          else
            text "Nee joh!"
            Game.move_user(user, prev_room)
        end
      end
    end

    room :exit do
      enter do
        text "Gefeliciteerd, je hebt de uitgang gevonden. Het licht doet pijn aan je ogen etc."
        if User.item_count(user, :diamond) >= 10 do
          text "Je hebt alle diamanten gevonden, wow!"
        end
        Game.game_over(user, :exit_found) # :died
      end
    end

Wat levert het op na compilatie?
Methodes worden aangeroepen met argumenten: user, room, item, prev_room/next_room
- move functies, elke richting is een methode die false of een room key teruggeeft:
  def move(:north), do: false
  def move(:east), do: :eetkamer
- initialize, bij begin nieuw spel: maak de kamer met objecten

Objecten

GameLogic

De speler bevindt zich altijd in een kamer.
De Game ontvangt commandos van de speler en verwerkt deze.
Bij het binnentreden of verlaten van een kamer worden de enter/leave functies van de kamer aangeroepen.
Bij het bekijken van een voorwerp wordt de look functie van een item aangeroepen.
Elke actie functie heeft als param de game state en retourneert de nieuwe gamestate.


Hoe sla je die kamers/voorwerpen op?
In een lijst met structs.

rooms = [
  %Room{id: :keuken, desc: "", enter: fn(game) -> ... end, north: false, east: :hal, ...}
]
items = [
  %Item{id: :lepel, use: fn(game) -> ...}
]

%Game{
  inventory: [],
  room: :keuken,
  prev_room: :hal,

}


Is het nog nodig om een DSL te maken? Bijna niet, het struct formaat is waarschijnlijk net zo 'compact', dus voor minder LOC hoeft het niet.

Eventuele voordelen?
- reverse exit check, heeft de kamer een weg terug (dit zou evt geen bezwaar hoeven te zijn)
