# Avontuur

An interactive console based adventure game.

I wrote this to create small adventures with my kids, so all the output is in Dutch, although the commands are also available in English.
Also, the game is not very complicated or refreshing.

There is an example adventure in `/adventures/testje.exs`. Run it with the helper script:

    ./run.sh testje

This should start the test adventure. Use the `help` command if your stuck.

Have fun writing and playing your own adventures!


## Installation

No installation needed, but you can compile the binary with:

    mix escript.build

And run your own adventure with:

    ./avontuur adventures/your_adventure.exs

Or just use the `run.sh` script to combine the two steps:

    ./run.sh your_adventure
